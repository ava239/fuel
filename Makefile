include make-compose.mk

start:
	php -t public -S localhost:8000

lint:
	./fuel/vendor/bin/fuelphpcs --ignore=bootstrap.php,views/* fuel/app

lint-fix:
	./fuel/vendor/bin/phpcbf --standard=ruleset.xml fuel/app
