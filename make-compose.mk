compose:
	heroku local -f Procfile.dev

compose-up:
	docker-compose up

compose-bash:
	docker-compose run app bash

compose-build:
	docker-compose build

compose-db:
	docker-compose exec db mysql -u test -h db --password=password1

compose-down:
	docker-compose down -v
