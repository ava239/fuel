<?php

class Observer_Article extends \Orm\Observer
{
	public function after_load(\Orm\Model $model)
	{
		$model->fixed = $model->content . ' changed by observer';
	}
}