<?php

class Controller_Routing extends \Controller_Template
{
	public $template = '404';

	private $routing = [
		'http://localhost:8000/'       => 'sites',
		'http://0.0.0.0:8000/'         => 'rest',
		'http://afuel.herokuapp.com/'  => 'sites',
		'https://afuel.herokuapp.com/' => 'rest',
		'default'                      => 'sites',
	];

	public function action_index()
	{
		$domain = Uri::base(false);
		if (in_array($domain, ['http://afuel.herokuapp.com/', 'https://afuel.herokuapp.com/'])) {
			//echo $domain;
		}
		$module = $this->routing[$domain] ?? $this->routing['default'];
		$isMainPage = Request::main()->uri->get() !== '';
		$uri = $isMainPage ? $module . '/' . Request::main()->uri : 'sites/home/index';
		try {
			$res = Request::forge($uri, $isMainPage)->execute();
			return $res->response();
		} catch (HttpNotFoundException $e) {
			print_r($e);
			return false;
		}
	}
}