<?php

class Model_Article_Category extends \Orm\Model
{
	protected static $_table_name = 'article_categories';
	protected static $_properties = ['id', 'title', 'content', 'slug'];
	protected static $_primary_key = ['id'];

	protected static $_has_many = [
		'articles' => [
			'key_from'       => 'id',
			'model_to'       => 'Model_Article',
			'key_to'         => 'category_id',
			'cascade_save'   => false,
			'cascade_delete' => false,
		]
	];
}