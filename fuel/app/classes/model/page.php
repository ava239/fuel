<?php

class Model_Page extends \Orm\Model
{
	protected static $_table_name = 'pages';
	protected static $_properties = ['id', 'title', 'content', 'slug'];
	protected static $_primary_key = ['id'];
}