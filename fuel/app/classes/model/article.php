<?php

class Model_Article extends \Orm\Model
{
	protected static $_table_name = 'articles';
	protected static $_properties = ['id', 'title', 'content', 'slug', 'category_id'];
	protected static $_primary_key = ['id'];

	protected static $_belongs_to = [
		'category' => [
			'key_from'       => 'category_id',
			'model_to'       => 'Model_Article_Category',
			'key_to'         => 'id',
			'cascade_save'   => false,
			'cascade_delete' => false,
		]
	];

	protected static $_observers = array(
		'article' => ['events' => ['after_load']],
	);
}