<?php
/**
 * Fuel is a fast, lightweight, community driven PHP 5.4+ framework.
 *
 * @package    Fuel
 * @version    1.8.2
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2019 Fuel Development Team
 * @link       https://fuelphp.com
 */

/**
 * -----------------------------------------------------------------------------
 *  Database settings for development environment
 * -----------------------------------------------------------------------------
 *
 *  These settings get merged with the global settings.
 *
 */
$url=parse_url(getenv("CLEARDB_DATABASE_URL"));

$server = $url["host"] ?? 'db';
$username = $url["user"] ?? 'test';
$password = $url["pass"] ?? 'password1';
$db = substr($url["path"],1);
if (!$db) {
	$db = 'test';
}

return array(
	'default' => array(
		'connection' => array(
			'dsn'      => "mysql:host={$server};dbname={$db}",
			'username' => $username,
			'password' => $password,
		),
	),
);
