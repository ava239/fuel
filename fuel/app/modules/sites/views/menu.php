<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
		aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarNav">
	<ul class="navbar-nav">
		<?php foreach ($menu as [$name, $link, $active]): ?>
			<li class="nav-item">
				<a class="nav-link <?php if($active):?>active<?php endif;?>" href="<?=$link?>"><?=$name?></a>
			</li>
		<?php endforeach; ?>
	</ul>
</div>