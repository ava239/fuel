<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $title; ?></title>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
          integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
          crossorigin="anonymous">
</head>
<body class="d-flex flex-column">
<header>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark">
        <a class="navbar-brand" href="/"><?php echo $title; ?></a>
        <?= $menu ?>
    </nav>
</header>

<main class="flex-grow-1">
	<?php if (Session::get_flash('success')): ?>
        <div class="alert alert-success">
            <strong>Success</strong>
            <p>
				<?php echo implode('</p><p>', e((array) Session::get_flash('success'))); ?>
            </p>
        </div>
	<?php endif; ?>
	<?php if (Session::get_flash('error')): ?>
        <div class="alert alert-danger">
            <strong>Error</strong>
            <p>
				<?php echo implode('</p><p>', e((array) Session::get_flash('error'))); ?>
            </p>
        </div>
	<?php endif; ?>
	<?php echo $content; ?>
</main>

<footer class="border-top py-3 mt-5">
    <div class="container-lg">
    </div>
</footer>
</body>
</html>