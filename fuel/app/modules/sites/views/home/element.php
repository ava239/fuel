<div class="container-lg">
    <h1 class="mt-5 mb-3"><?= $element->category->title ?> / <?= $element->title ?></h1>
    <div>
        <p><?= $element->content ?></p>
        <div><?= $element->fixed ?></div>
    </div>
</div>
