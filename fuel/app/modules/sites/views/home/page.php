<div class="container-lg">
    <h1 class="mt-5 mb-3"><?= $page->title ?></h1>
    <div>
        <?= $page->content ?>
    </div>
</div>
