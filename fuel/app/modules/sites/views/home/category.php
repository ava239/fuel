<div class="container-lg">
    <h1 class="mt-5 mb-3"><?= $category->title ?></h1>
    <div>
        <?= $category->content ?>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover text-nowrap">
            <tr>
                <th>ID</th>
                <th>Название</th>
            </tr>
            <?php foreach ($category->articles as $article): ?>
            <tr>
                <th><?= $article->id ?></th>
                <td>
                    <a href="<?= Router::get('element', ['category'=>$category->slug, 'element' => $article->slug]) ?>">
                        <?= $article->title ?>
                    </a>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>
