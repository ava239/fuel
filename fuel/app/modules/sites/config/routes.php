<?php
/**
 * Fuel is a fast, lightweight, community driven PHP 5.4+ framework.
 *
 * @package   Fuel
 * @version   1.8.2
 * @author    Fuel Development Team
 * @license   MIT License
 * @copyright 2010 - 2019 Fuel Development Team
 * @link      https://fuelphp.com
 */

return array(
	'page(/:slug)' => array(
		'sites/home/$2', 'name' => 'page',
	),
	'(:category)/(:element)' => array(
		'sites/articles/show', 'name' => 'element',
	),
	'(:category)' => array(
		'sites/articles/index', 'name' => 'category',
	),
);
