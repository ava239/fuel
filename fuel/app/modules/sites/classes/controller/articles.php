<?php

namespace Sites;

class Controller_Articles extends \Controller_Template
{
	public $template = 'layout';

	public function action_index()
	{
		$category = \Model_Article_Category::find('first', [
			'where' => [
				['slug', $this->param('category')]
			]
		]);
		$this->template->title = $category->title;
		$data = [
			'category' => $category,
		];
		$this->template->content = \View::forge('home/category', $data);
	}

	public function action_show()
	{
		$element = \Model_Article::find('first', [
			'where'   => [
				['slug', $this->param('element')]
			],
			'related' => [
				'category' => [
					'where' => [
						['slug', $this->param('category')]
					]
				]
			]
		]);
		$this->template->title = $element->title;
		$data = [
			'element'  => $element,
		];
		$this->template->content = \View::forge('home/element', $data);
	}

	public function before()
	{
		parent::before();
		$this->template->content = '';
		$this->template->menu = \Presenter::forge('menu');
	}
}