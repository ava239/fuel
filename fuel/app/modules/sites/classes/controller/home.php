<?php

namespace Sites;

class Controller_Home extends \Controller_Template
{
	public $template = 'layout';

	public function action_index()
	{
		$this->template->title = 'test';
	}

	public function action_page1()
	{
		$this->template->title = 'page1';
		$data = [
			'page' => \Model_Page::find('first', [
				'where' => [
					['slug', 'page1']
				]
			]),
		];

		$this->template->content = \View::forge('home/page', $data);
	}

	public function action_page2()
	{
		$this->template->title = 'page2';
		$data = [
			'page' => \Model_Page::find('first', [
				'where' => [
					['slug', 'page2']
				]
			]),
		];
		$this->template->content = \View::forge('home/page', $data);
	}

	public function action_404()
	{
		return \Response::forge(\View::forge('404'), 404);
	}

	public function before()
	{
		parent::before();
		$this->template->title = '';
		$this->template->content = '';
		$this->template->menu = \Presenter::forge('menu');
	}
}