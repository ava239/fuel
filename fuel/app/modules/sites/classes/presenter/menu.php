<?php

namespace Sites;

class Presenter_Menu extends \Presenter
{
	public function view()
	{
		$isActive = function ($routeName, $paramName = 'slug') {
			$routeParams = \Request::active()->route->named_params;
			$name = $routeParams[$paramName] ?? 'index';
			return $routeName === $name;
		};
		$removeModulePath = function ($url) {
			$parsedUrl = parse_url($url);
			$path = explode('/', $parsedUrl['path']);
			$host = $parsedUrl['host'];
			$host .= ($parsedUrl['port'] ?? false) ? ':' . $parsedUrl['port'] : '';
			$newPath = implode('/', array_slice($path, 2));
			return strtolower($parsedUrl['scheme'] . '://' . $host . '/' . $newPath);
		};
		$this->menu = [
			[
				'Главная',
				'/',
				$isActive('index')
			],
			[
				'Страница 1',
				$removeModulePath(\Router::get('page', ['/page1'])),
				$isActive('page1')
			],
			[
				'Страница 2',
				$removeModulePath(\Router::get('page', ['/page2'])),
				$isActive('page2')
			],
			[
				'Новости',
				$removeModulePath(\Router::get('category', ['news'])),
				$isActive('news', 'category')
			],
		];
	}
}
