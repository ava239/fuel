<?php

namespace Rest;

class Controller_Articles extends \Controller_rest
{
	public function get_index()
	{
		return $this->get_articles();
	}

	public function get_categories()
	{
		return $this->response([
			'categories' => \Model_Article_Category::find('all'),
		]);
	}

	public function get_articles()
	{
		return $this->response([
			'elements' => \Model_Article::find('all'),
		]);
	}

	public function get_article()
	{
		return $this->response(\Model_Article::find($this->param('id')));
	}

	public function put_article()
	{
		$data = \Input::put();
		$obj = \Model_Article::forge($data);
		$obj->save();
		return $this->response($obj->id);
	}

	public function delete_article()
	{
		$article = \Model_Article::find($this->param('id'));
		if (!$article) {
			return $this->response('no object', 404);
		}
		$article->delete();
		return $this->response('ok', 200);
	}
}